#' Charger un fichier sitadel
#'
#' @param file l'emplacement du fichier
#'
#' @return un dataframe
#' @export
#' @importFrom readxl read_excel
#' @importFrom tricky set_standard_names
#' @importFrom dplyr rename select filter mutate mutate_if
#' @importFrom tidyr pivot_longer pivot_wider complete
creer_millesime_sitadel <- function(file) {
  sitadel_logement <- readxl::read_excel(file, sheet = 1) %>%
    tricky::set_standard_names() %>%
    dplyr::rename(
      date = .data$lgt_annee,
      depcom = .data$lgt_code_de_la_commune
    )

  sitadel_locaux <- readxl::read_excel(file, sheet = 2) %>%
    tricky::set_standard_names() %>%
    dplyr::rename(
      date = .data$loc_annee,
      depcom = .data$loc_code_de_la_commune
    )

  sitadel <- dplyr::bind_rows(
    sitadel_logement %>%
      tidyr::pivot_longer(names_to = "variable", values_to = "valeur", .data$nb_lgt_aut_ordinaires:.data$nb_lgt_com_total),
    sitadel_locaux %>%
      tidyr::pivot_longer(names_to  = "variable", values_to = "valeur", .data$surface_aut_creee_totale_des_locaux:.data$surface_com_creee_loc_total_service_public)
  ) %>%
    dplyr::select(.data$depcom, .data$date, .data$variable, .data$valeur) %>%
    dplyr::filter(.data$date >= 2008) %>%
    dplyr::mutate(date = lubridate::make_date(.data$date, 12, 31)) %>%
    dplyr::mutate_if(is.character, as.factor) %>%
    tidyr::complete(.data$depcom, .data$date, .data$variable, fill = list(valeur = 0)) %>% 
    tidyr::pivot_wider(names_from = "variable",values_from = "valeur")

  return(sitadel)
}


#' charger l'ensemble des fichiers sitadel
#'
#' @param file_path le répertoire avec les fichers extraits de géokit 3
#'
#' @return un dataframe
#' @export
#' @importFrom purrr map_dfr
creer_sitadel <- function(file_path = "extdata/sitadel") {
  liste_fichier_sitadel <- list.files(file_path,full.names = TRUE)
  sitadel <- purrr::map_dfr(liste_fichier_sitadel, ~ creer_millesime_sitadel(.x))
}
